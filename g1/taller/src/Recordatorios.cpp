#include <iostream>
#include <list>

using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10

// Clase Fecha
class Fecha {
  public:
    Fecha(int mes, int dia);
    int mes();
    int dia();
    void incrementar_dia();
    // Completar declaraciones funciones
    #if EJ >= 9 // Para ejercicio 9
    bool operator==(Fecha o);
    #endif

  private:
    int fecha_;
    int mes_;
    //Completar miembros internos
};

Fecha::Fecha(int mes, int dia): fecha_ (dia), mes_ (mes){};

int Fecha::mes(){
    return mes_;
}

int Fecha::dia(){
    return fecha_;
}

ostream& operator<<(ostream& os, Fecha f) {
    os << f.dia() << "/" << f.mes();
    return os;
}

#if EJ >= 9
bool Fecha::operator==(Fecha o) {
    bool igual_dia = this->dia() == o.dia();
    bool igual_mes = this->mes() == o.mes();
    return igual_dia && igual_mes;
}
#endif

void Fecha::incrementar_dia(){
    if(fecha_ == dias_en_mes(mes_) && mes_ == 11){
        fecha_ = 1;
        mes_ = 0;
    }else if(fecha_ == dias_en_mes(mes_)){
        fecha_ = 1;
        mes_ ++;
    }else{
        fecha_++;
    }
}
// Ejercicio 11, 12

// Clase Horario
class Horario{
    public:
        Horario(uint hora, uint min);
        uint hora();
        uint min();
        #if EJ == 11 // Para ejercicio 11
            bool operator==(Horario h);
        #endif
        bool operator<(Horario h);

    private:
        uint hora_;
        uint min_;
};

Horario::Horario(uint hora, uint min): hora_(hora), min_(min) {};

uint Horario::hora(){
    return hora_;
}

uint Horario::min(){
    return min_;
}

#if EJ == 11
bool Horario::operator==(Horario h) {
    bool igual_hora = this->hora() == h.hora();
    bool igual_min = this->min() == h.min();
    return igual_hora && igual_min;
}
#endif

ostream& operator<<(ostream& os, Horario h) {
    os << h.hora() << ":" << h.min();
    return os;
}

bool Horario::operator<(Horario h) {
    bool menor_hora = this->hora() <= h.hora();
    bool menor_min = this->min() < h.min();
    return menor_hora && menor_min;
}

// Ejercicio 13

// Clase Recordatorio
class Recordatorio{
    public:
        Recordatorio(Fecha f, Horario h, string rec);
        string rec();
        Horario horario_rec();
        Fecha fecha_rec();


    private:
        string rec_;
        Fecha fecha_rec_;
        Horario horario_rec_;
};

Recordatorio::Recordatorio(Fecha f, Horario h, string rec): fecha_rec_(f), horario_rec_(h), rec_(rec)  {};

bool operator<(Recordatorio rec1, Recordatorio rec2){
    if(rec1.h_rec() <= rec2.h_rec()){
        return (rec1.min_rec() <= rec2.min_rec());
    }
    return false;
}
string Recordatorio::rec() {
    return rec_;
}

Fecha Recordatorio::fecha_rec() {
    return fecha_rec_;
}

Horario Recordatorio::horario_rec() {
    return horario_rec_;
}

ostream& operator<<(ostream& os, Recordatorio r) {
    os << r.rec() << " @ " << r.fecha_rec() << " " << r.horario_rec();
    return os;
}

// Ejercicio 14

// Clase Agenda

class Agenda {
    public:
        Agenda(Fecha fecha_inicial);
        void agregar_recordatorio(Recordatorio recNuevo);
        void incrementar_dia();
        list<Recordatorio> recordatorios_de_hoy();
        Fecha hoy();

    private:
        Fecha fecha_inicial_;
        Fecha fecha_actual_ = fecha_inicial_;
        list<Recordatorio> recordatorios;

};

Agenda::Agenda(Fecha fecha_inicial): fecha_inicial_ (fecha_inicial) {};

void Agenda::agregar_recordatorio(Recordatorio recNuevo) {
    list<Recordatorio> ordenarRec;
    bool colocado = false;
    if(recordatorios.empty()){
        ordenarRec.push_back(recNuevo);
        colocado = true;
    }for(Recordatorio recViejo: recordatorios) {
        if (recViejo < recNuevo) {
            ordenarRec.push_back(recViejo);
        } else if (!(recViejo < recNuevo) && not colocado) {
            ordenarRec.push_back(recNuevo);
            colocado = true;
            ordenarRec.push_back(recViejo);
        } else {
            ordenarRec.push_back(recViejo);
        }
    }if(not colocado){
        ordenarRec.push_back(recNuevo);
        colocado = true;
    }
    recordatorios = ordenarRec;
};

void Agenda::incrementar_dia(){
    fecha_actual_.incrementar_dia();
}

list<Recordatorio> Agenda::recordatorios_de_hoy(){
    list<Recordatorio> rec_de_hoy;
    for(Recordatorio rec: recordatorios) {
        if (rec.fecha_rec() == fecha_actual_) {
            rec_de_hoy.push_back(rec);
        }
    }
    return rec_de_hoy;
};

Fecha Agenda::hoy() {
    return fecha_actual_;
}

ostream& operator<<(ostream& os, Agenda a) {
    os << a.hoy().dia() << "/" << a.hoy().mes() << endl;
    os << "=====" << endl;
    for(Recordatorio rec: a.recordatorios_de_hoy()) {
        os << rec.rec() << " @ " << rec.dia_rec() << "/" << rec.mes_rec() << " " <<
        rec.h_rec()<< ":" << rec.min_rec() << endl;
    }
    return os;
}