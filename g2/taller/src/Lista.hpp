#include "Lista.h"

Lista::Lista() {
    prim = NULL;
    ult = NULL;
}

Lista::Lista(const Lista& l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    *this = l;
}

Lista::~Lista() {
    this->destruir();
}

Lista& Lista::operator=(const Lista& aCopiar) {
    this->destruir();
    Nodo* actual = aCopiar.prim;
    while(actual != nullptr){
        agregarAtras(actual->data);
        actual = actual->sig;
    }
    return *this;
}

void Lista::agregarAdelante(const int& elem) {
    Nodo* tmp = new Nodo;
    tmp->data = elem;
    tmp->ant = NULL;
    if(prim != nullptr){
        tmp->sig = prim;
        prim->ant = tmp;
    }else{
        tmp->sig = NULL;
        ult = tmp;
    }
    prim = tmp;
    _longitud++;
}

void Lista::agregarAtras(const int& elem) {
    Nodo* tmp = new Nodo;
    tmp->data = elem;
    tmp->sig = NULL;
    if(ult != nullptr){
        tmp->ant = ult;
        ult->sig = tmp;
    }else{
        tmp->ant = NULL;
        prim = tmp;
    }
    ult = tmp;
    _longitud++;
}

void Lista::eliminar(Nat i) {
    if(i <= _longitud){
        int longitud = 0;
        Nodo* actual = prim;
        while(longitud < i) {
            longitud++;
            actual = actual->sig;
        }if(_longitud == 1){
            prim = nullptr;
            ult = nullptr;
        }else if(actual->sig == nullptr){
            actual->ant->sig = NULL;
            ult = actual->ant;
        }else if(actual->ant == nullptr){
            actual->sig->ant = NULL;
            prim = actual->sig;
        }else{
            actual->ant->sig = actual->sig;
            actual->sig->ant = actual->ant;
        }
        delete actual;
        _longitud--;
    }
}

int Lista::longitud() {
//    int longitud = 0;
//    if(prim == nullptr){
//        return 0;
//    }else{
//        Nodo* actual = prim;
//        while(actual != nullptr){
//            longitud++;
//            actual = actual->sig;
//        }
//    }
//    return longitud;
    return _longitud;
}

const int& Lista::iesimo(Nat i) const {
    int longitud = 0;
    Nodo* actual = prim;
    while(longitud < i){
        longitud++;
        actual = actual->sig;
    }
    return actual->data;
}

int& Lista::iesimo(Nat i) {
    int longitud = 0;
    Nodo* actual = prim;
    while(longitud < i){
        longitud++;
        actual = actual->sig;
    }
    return actual->data;
}

void Lista::mostrar(ostream& o) {
    o << "[";
    for(int i = 0; i < _longitud; i++){
        o << iesimo(i);
        if(i < _longitud - 1){
            o << ",";
        }
    }
    o << "]";
}

void Lista::destruir(){
    while(_longitud > 0){
        Nodo* actual = prim;
        prim = actual->sig;
        actual->sig = nullptr;
        delete actual;
        _longitud--;
    }
    ult = nullptr;
}

//int main(){
//        Lista l;
//        l.agregarAtras(42);
//        l.agregarAtras(43);
//        l.agregarAtras(44);
//        l.agregarAtras(45);
//        l.destruir();
//};