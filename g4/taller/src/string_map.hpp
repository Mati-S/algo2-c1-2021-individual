
#include "string_map.h"

template<typename T>
string_map<T>::string_map() {
    raiz = nullptr;
    _size = 0;
}

template<typename T>
string_map<T>::string_map(const string_map<T> &aCopiar)
        : string_map() { *this = aCopiar; } // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.

template<typename T>
string_map<T> &string_map<T>::operator=(const string_map<T> &d) {
    if(d.raiz != nullptr){
        Nodo* raizAcrear = new Nodo(nullptr);
        copiarTrie(raizAcrear, d.raiz);
        raiz = raizAcrear;
        _size = d._size;
    }
    return *this;
}

template<typename T>
void string_map<T>::copiarTrie(Nodo* t, Nodo* s) {
    for (int i = 0; i < s->siguientes.size(); i++) {
        Nodo* posicionNodo = s->siguientes[i];
        if (posicionNodo != nullptr) {
            if (posicionNodo->definicion != nullptr) {
                T valor = *posicionNodo->definicion;
                Nodo* tmp = new Nodo(new T(valor));
                t->siguientes[i] = tmp;
                copiarTrie(t->siguientes[i], posicionNodo);
            } else {
                Nodo *tmp = new Nodo(nullptr);
                t->siguientes[i] = tmp;
                copiarTrie(t->siguientes[i], posicionNodo);
            }
        }
    }
}


template<typename T>
string_map<T>::~string_map() {
    if (raiz != nullptr) {
        destruirArbol(raiz);
    }else{
        delete raiz;
    }
}


template<typename T>
void string_map<T>::destruirArbol(Nodo *t) {
    for (Nodo* nodos: t->siguientes) {
        if (nodos != nullptr) {
            destruirArbol(nodos);
        }else{
            delete nodos;
        }
    }
    delete t->definicion;
    delete t;
    _size = _size - 1;
}

template<typename T>
void string_map<T>::insert(const pair<string, T> &p) {
    if (raiz == nullptr) {
        Nodo* tmp = new Nodo(nullptr);
        raiz = tmp;
    }
    bool existe = count(p.first);
    string clave = p.first;
    Nodo* actual = raiz;
    if(existe){
        this->at(clave) = p.second;
    }else{
        T* valor = new T (p.second);
        for (int i = 0; i < clave.length(); i++) {
            int letra = int(clave[i]);
            if (i == clave.length() - 1) {
                if (actual->siguientes[letra] == nullptr) {
                    Nodo* tmp = new Nodo(valor);

                    actual->siguientes[letra] = tmp;
                } else {
                    actual = actual->siguientes[letra];
                    actual->definicion = valor;
                }
            } else if (actual->siguientes[letra] == nullptr) {
                Nodo* tmp = new Nodo(nullptr);
                actual->siguientes[letra] = tmp;
                actual = actual->siguientes[letra];
            } else {
                actual = actual->siguientes[letra];
            }
        }
        _size = _size + 1;
    }
}

template<typename T>
T &string_map<T>::operator[](const string &clave) {
    // COMPLETAR
}


template<typename T>
int string_map<T>::count(const string &clave) const {
    if (raiz == nullptr) {
        return 0;
    } else {
        Nodo* actual = raiz;
        for (int i = 0; i < clave.length(); i++) {
            int letra = int(clave[i]);
            if (actual->siguientes[letra] == nullptr) {
                return 0;
            } else {
                actual = actual->siguientes[letra];
            }
        }
        return actual->definicion != nullptr;
    }
}

template<typename T>
const T &string_map<T>::at(const string &clave) const {
    Nodo* actual = raiz;
    for (int i = 0; i < clave.length(); i++) {
        int letra = int(clave[i]);
        actual = actual->siguientes[letra];
    }
    return *actual->definicion;
}

template<typename T>
T &string_map<T>::at(const string &clave) {
    Nodo* actual = raiz;
    for (int i = 0; i < clave.length(); i++) {
        int letra = int(clave[i]);
        actual = actual->siguientes[letra];
    }
    return *actual->definicion;
}

template<typename T>
void string_map<T>::erase(const string &clave) {
    eliminarHoja(raiz, clave, 0);
    _size = _size - 1;
}

template<typename T>
void  string_map<T>::eliminarHoja (Nodo* t,string clave, int i){
    int posicion = int(clave[i]);
    Nodo* siguiente = t->siguientes[posicion];
    if(i == clave.length() - 1){
        delete siguiente->definicion;
        siguiente->definicion = nullptr;
        if(esHoja(siguiente)) {
            delete siguiente;
            t->siguientes[posicion] = nullptr;
        }
    }else{
        eliminarHoja(siguiente, clave, i + 1);
        if(t != raiz && esHoja(t)){
            delete siguiente;
            t->siguientes[posicion] = nullptr;
        }else{
            if(esHoja(t)){
                delete siguiente;
                t->siguientes[posicion] = nullptr;
            }
        }
    }
}


template<typename T>
int string_map<T>::size() const {
    return _size;
}

template<typename T>
bool string_map<T>::empty() const {
    return size() == 0;
}

template<typename T>
bool string_map<T>::esHoja(Nodo *t) {
    for (Nodo* nodos: t->siguientes){
        if(nodos != nullptr){
            return false;
        }
    }
    return true;
}