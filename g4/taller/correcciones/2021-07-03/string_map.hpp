
#include "string_map.h"

template<typename T>
string_map<T>::string_map() {
    raiz = nullptr;
    _size = 0;
}

template<typename T>
string_map<T>::string_map(const string_map<T> &aCopiar)
        : string_map() { *this = aCopiar; } // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.

template<typename T>
string_map<T> &string_map<T>::operator=(const string_map<T> &d) {
    if(d.raiz != nullptr){
        Nodo* raizAcrear = new Nodo();
        Nodo* raizAcompiar = d.raiz;
        for (int i = 0; i < raizAcompiar->siguientes.size(); i++) {
            Nodo* posicionNodo = raizAcompiar->siguientes[i];
            if (posicionNodo != nullptr) {
                if (posicionNodo->definicion != nullptr) {
                    T valor = *posicionNodo->definicion;
                    Nodo* tmp = new Nodo(new T(valor));
                    tmp->hijos = posicionNodo->hijos;
                    raizAcrear->siguientes[i] = tmp;
                    copiarTrie(raizAcrear->siguientes[i], posicionNodo);
                } else {
                    Nodo* tmp = new Nodo();
                    tmp->hijos = posicionNodo->hijos;
                    raizAcrear->siguientes[i] = tmp;
                    copiarTrie(raizAcrear->siguientes[i], posicionNodo);
                }
            }
        }
        raiz = raizAcrear;
        _size = d._size;
    }
}

template<typename T>
string_map<T>::~string_map() {
    if (raiz != nullptr) {
        for (Nodo* nodos: raiz->siguientes) {
            if (nodos != nullptr) {
                destruirArbol(nodos);
            }
        }
    }
    delete raiz;
}

template<typename T>
void string_map<T>::insert(const pair<string, T> &p) {
    if (raiz == nullptr) {
        Nodo* tmp = new Nodo();
        raiz = tmp;
    }
    bool existe = count(p.first);
    T valor = p.second;
    string clave = p.first;
    Nodo* actual = raiz;
    if(existe){
        this->at(clave) = valor;
    }else{
        for (int i = 0; i < clave.length(); i++) {
            int letra = int(clave[i]);
            if (i == clave.length() - 1) {
                if (actual->siguientes[letra] == nullptr) {
                    actual->hijos = actual->hijos + 1;
                    Nodo* tmp = new Nodo(new T(valor));
                    actual->siguientes[letra] = tmp;
                } else {
                    actual = actual->siguientes[letra];
                    actual->hijos = actual->hijos + 1;
                    actual->definicion = new T(valor);
                }
            } else if (actual->siguientes[letra] == nullptr) {
                Nodo* tmp = new Nodo();
                actual->hijos = actual->hijos + 1;
                actual->siguientes[letra] = tmp;
                actual = actual->siguientes[letra];
            } else {
                actual->hijos = actual->hijos + 1;
                actual = actual->siguientes[letra];
            }
        }
        _size = _size + 1;
        raiz->hijos = _size;
    }
}

template<typename T>
T &string_map<T>::operator[](const string &clave) {
    // COMPLETAR
}


template<typename T>
int string_map<T>::count(const string &clave) const {
    if (raiz == nullptr) {
        return 0;
    } else {
        Nodo* actual = raiz;
        for (int i = 0; i < clave.length(); i++) {
            int letra = int(clave[i]);
            if (actual->siguientes[letra] == nullptr) {
                return 0;
            } else {
                actual = actual->siguientes[letra];
            }
        }
        return actual->definicion != nullptr;
    }
}

template<typename T>
const T &string_map<T>::at(const string &clave) const {
    Nodo* actual = raiz;
    for (int i = 0; i < clave.length(); i++) {
        int letra = int(clave[i]);
        actual = actual->siguientes[letra];
    }
    return *actual->definicion;
}

template<typename T>
T &string_map<T>::at(const string &clave) {
    Nodo* actual = raiz;
    for (int i = 0; i < clave.length(); i++) {
        int letra = int(clave[i]);
        actual = actual->siguientes[letra];
    }
    return *actual->definicion;
}

template<typename T>
void string_map<T>::erase(const string &clave) {
    eliminarHoja(raiz, clave, 0);
    _size = _size - 1;
}

template<typename T>
int string_map<T>::size() const {
    return _size;
}

template<typename T>
bool string_map<T>::empty() const {
    return size() == 0;
}


template<typename T>
void string_map<T>::destruirArbol(Nodo *t) {
    for (Nodo* nodos: t->siguientes) {
        if (nodos != nullptr) {
            destruirArbol(nodos);
        }
    }
    if (t->definicion != nullptr) {
        t->definicion = nullptr;
    }
    delete t;
    _size = _size - 1;
}

template<typename T>
void string_map<T>::copiarTrie(Nodo* t, Nodo* s) {
    for (int i = 0; i < s->siguientes.size(); i++) {
        t->hijos = s->hijos;
        Nodo* posicionNodo = s->siguientes[i];
        if (posicionNodo != nullptr) {
            if (posicionNodo->definicion != nullptr) {
                T valor = *posicionNodo->definicion;
                Nodo* tmp = new Nodo(new T(valor));
                t->siguientes[i] = tmp;
                copiarTrie(t->siguientes[i], posicionNodo);
            } else {
                Nodo *tmp = new Nodo();
                t->siguientes[i] = tmp;
                copiarTrie(t->siguientes[i], posicionNodo);
            }
        }
    }
}


template<typename T>
void  string_map<T>::eliminarHoja (Nodo* t,string clave, int i){
    int posicion = int(clave[i]);
    Nodo* siguiente = t->siguientes[posicion];
    t->hijos = t->hijos - 1;
    if(i == clave.length() - 1){
        siguiente->hijos = siguiente->hijos - 1;
        siguiente->definicion = nullptr;
        if(siguiente->hijos == 0) {
            delete siguiente;
            t->siguientes[posicion] = nullptr;
        }
    }else{
         if(t->hijos >= 1){
             eliminarHoja(siguiente, clave, i + 1);
         }else{
             eliminarHoja(siguiente, clave, i + 1);
             if(t != raiz){
                 delete siguiente;
                 t->siguientes[posicion] = nullptr;
             }else{
                 if(_size == 0){
                     delete siguiente;
                     t->siguientes[posicion] = nullptr;
                 }
             }
         }
    }
}

//
//int main (){
//    string_map<int> a1;
//    string_map<string_map<int>> a2;
//
//    a2.insert(make_pair("a1", a1));
//    a1.insert(make_pair("0", 1));
//    a1.insert(make_pair("01", 5));
//    a1.insert(make_pair("010", 2));
//    a2.insert(make_pair("a1", a1));
//
//    a2.at("a1").erase("010");
//    a1.erase("01");
//}