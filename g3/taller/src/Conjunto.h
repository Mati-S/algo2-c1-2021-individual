#ifndef CONJUNTO_H_
#define CONJUNTO_H_

#include <assert.h>
#include <string>
#include <iostream>

using namespace std;

template <class T>
class Conjunto
{
    public:

        // Constructor. Genera un conjunto vacío.
        Conjunto();

        // Destructor. Debe dejar limpia la memoria.
        ~Conjunto();

        // Inserta un elemento en el conjunto. Si este ya existe,
        // el conjunto no se modifica.
        void insertar(const T&);

        // Decide si un elemento pertenece al conjunto o no.
        bool pertenece(const T&) const;

        // Borra un elemento del conjunto. Si este no existe,
        // el conjunto no se modifica.
        void remover(const T&);

        // Siguiente elemento al recibido por párametro, en orden.
        const T& siguiente(const T& elem);

        // Devuelve el mínimo elemento del conjunto según <.
        const T& minimo() const;

        // Devuelve el máximo elemento del conjunto según <.
        const T& maximo() const;

        // Devuelve la cantidad de elementos que tiene el conjunto.
        unsigned int cardinal() const;

        // Muestra el conjunto.
        void mostrar(std::ostream&) const;




    private:
        /**
         * Completar con lo que sea necesario...
         **/
        int nodos = 0;

        struct Nodo
        {
            // El constructor, toma el elemento al que representa el nodo.
            Nodo(const T& v);
            // El elemento al que representa el nodo.
            T valor;
            // Puntero a la raíz del subárbol izquierdo.
            Nodo* izq;
            // Puntero a la raíz del subárbol derecho.
            Nodo* der;

            Nodo* padre;

            int hijos;

        };

        // Puntero a la raíz de nuestro árbol.
        Nodo* _raiz;

    void destruir(Nodo *n);

    Nodo* sucesor(Nodo*);

    Nodo* antecesor(Nodo*);

    Nodo* encontrarNodo (Nodo* n, const T& elem) const;

    bool esHoja (Nodo* n);

    void eliminarRama(Nodo* n);

    void eliminarHoja(Nodo* n);

};

template<class T>
Conjunto<T>::Nodo::Nodo(const T &v): padre(nullptr), valor(v), izq(nullptr), der(nullptr), hijos(0){}


template<class T>
bool Conjunto<T>::esHoja(Nodo *n) {
    return n->hijos == 0;
}

template<class T>
typename Conjunto<T>::Nodo* Conjunto<T>::sucesor(Nodo* n){
    Nodo* suplente = n->der;
    while (suplente->izq != nullptr) {
        suplente = suplente->izq;
    }
    return suplente;
}

template<class T>
typename Conjunto<T>::Nodo* Conjunto<T>::antecesor(Nodo* n){
    Nodo* suplente = n->izq;
    while (suplente->der != nullptr) {
        suplente = suplente->der;
    }
    return suplente;
}


template<class T>
std::ostream& operator<<(std::ostream& os, const Conjunto<T>& c) {
	 c.mostrar(os);
	 return os;
}

template<class T>
typename Conjunto<T>::Nodo* Conjunto<T>::encontrarNodo(Nodo* n, const T& elem) const{
    if(n->valor == elem){
        return n;
    }else if(n->valor < elem){
        if(n->der != nullptr){
            return encontrarNodo(n->der, elem);
        }else{
            return n;
        }
    }else{
        if(n->izq != nullptr){
            return encontrarNodo(n->izq, elem);
        }else{
            return n;
        }
    }
}

template<class T>
void Conjunto<T>::eliminarRama(Nodo* n){
    Nodo* suplente;
    Nodo* padre = n->padre;
    if(n->der != nullptr){
        suplente = n->der;
    }else{
        suplente = n->izq;
    }if(padre != nullptr){
        if(padre->valor < suplente->valor){
            padre->der = suplente;
        }else {
            padre->izq = suplente;
        }
        suplente->padre = padre;
    }else{
        _raiz = suplente;
    }
    delete (n);
}

template<class T>
void Conjunto<T>::eliminarHoja(Nodo* n) {
    Nodo* padre = n->padre;
    const T& valorP = padre->valor;
    padre->hijos = padre->hijos - 1;
    if(valorP > n->valor or padre->der == nullptr){
            padre->izq = nullptr;
    }else if (valorP < n->valor or padre->izq == nullptr){
        padre->der = nullptr;
    }else if (padre->izq != n) {
        padre->der = nullptr;
    }else{
        padre->izq = nullptr;
    }
    delete n;
}

#include "Conjunto.hpp"

#endif // CONJUNTO_H_
