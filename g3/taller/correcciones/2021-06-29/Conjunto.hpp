
#include "Conjunto.h"

template <class T>
Conjunto<T>::Conjunto() {
    _raiz = nullptr;
}

template <class T>
Conjunto<T>::~Conjunto() {
    destruir(_raiz);
}


template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {
    Nodo* actual = _raiz;
    if(actual == nullptr){
        return false;
    }else{
        actual = encontrarNodo(_raiz, clave);
        return actual->valor == clave;
    }
}

template <class T>
void Conjunto<T>::insertar(const T& clave) {
    if(_raiz == nullptr){
        Nodo* tmp = new Nodo(clave);
        _raiz = tmp;
        nodos++;
    }else{
        Nodo* actual = encontrarNodo(_raiz, clave);
        if (actual->valor > clave){
            Nodo* tmp = new Nodo(clave);
            nodos++;
            actual->hijos = actual->hijos + 1;
            tmp->padre = actual;
            actual->izq = tmp;
        }else if(actual->valor < clave) {
            Nodo* tmp = new Nodo(clave);
            nodos++;
            actual->hijos = actual->hijos + 1;
            tmp->padre = actual;
            actual->der = tmp;
        }
    }
}

template <class T>
void Conjunto<T>::remover(const T& clave) {
    Nodo* actual = encontrarNodo(_raiz, clave);
    nodos--;
    if(actual == _raiz && esHoja(actual)){
        delete _raiz;
        _raiz = nullptr;
    }else if(esHoja(actual)) {
        eliminarHoja(actual);
    }else if(actual->hijos == 1){
        eliminarRama(actual);
    }else{
        Nodo *nietoMenor = antecesor(actual);
        Nodo *nietoMayor = sucesor(actual);
        if (not esHoja(nietoMayor)) {
            if (esHoja(nietoMenor)) {
                actual->valor = nietoMenor->valor;
                eliminarHoja(nietoMenor);
            }else{
                actual->valor = nietoMayor->valor;
                eliminarRama(nietoMayor);
            }
        }else{
            actual->valor = nietoMayor->valor;
            eliminarHoja(nietoMayor);
        }
    }
}


template <class T>
const T& Conjunto<T>::siguiente(const T& clave) {
    Nodo* actual = _raiz;
    bool encontro = false;
    while(not encontro) {
        if (actual->valor < clave) {
            actual->hijos = actual->hijos - 1;
            actual = actual->der;
        } else if (actual->valor > clave) {
            actual->hijos = actual->hijos - 1;
            actual = actual->izq;
        } else if (actual->valor == clave) {
            encontro = true;
        }
    }
    if(actual->der != nullptr){
        actual = actual->der;
        if (actual == nullptr){
            return _raiz->valor;
        }else{
            while(actual->izq != nullptr){
                actual = actual->izq;
            }
            return actual->valor;
        }
    }else{
        actual = actual->padre;
        return actual->valor;
    }
}

template <class T>
const T& Conjunto<T>::minimo() const {
    if (_raiz == nullptr){
        return _raiz->valor;
    }else{
        Nodo* actual = _raiz;
        while(actual->izq != nullptr){
            actual = actual->izq;
        }
        return actual->valor;
    }
}

template <class T>
const T& Conjunto<T>::maximo() const {
    if (_raiz == nullptr){
        return _raiz->valor;
    }else{
        Nodo* actual = _raiz;
        while(actual->der != nullptr){
            actual = actual->der;
        }
        return actual->valor;
    }
}

template <class T>
unsigned int Conjunto<T>::cardinal() const {
    return nodos;
}

template <class T>
void Conjunto<T>::mostrar(std::ostream&) const {
    assert(false);
}

template <class T>
void Conjunto<T>::destruir(struct Nodo* n) {
    if (n != nullptr) {
        if(n->izq != nullptr){
            destruir(n->izq);
        }if(n->der != nullptr){
            destruir(n->der);
        }
        delete n;
    }
}

