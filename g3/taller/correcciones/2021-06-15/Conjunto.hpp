
#include "Conjunto.h"

template <class T>
Conjunto<T>::Conjunto() {
    _raiz = nullptr;
}

template <class T>
Conjunto<T>::~Conjunto() {
    destruir(_raiz);
}


template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {
    Nodo* actual = _raiz;
    while(actual != nullptr and actual->valor != clave) {
         if (actual->valor < clave) {
            actual = actual->der;
        }else{
            actual = actual->izq;
        }
    }if(actual == nullptr){
        return false;
    }
    return actual->valor == clave;
}

template <class T>
void Conjunto<T>::insertar(const T& clave) {
    Nodo* tmp = new Nodo(clave);
    if(_raiz == nullptr){
        _raiz = tmp;
        nodos++;
    }else{
        Nodo* actual = _raiz;
        while (actual->valor != clave) {
            if (actual->valor > clave){
                if(actual->izq == nullptr) {
                    tmp->padre = actual;
                    actual->izq = tmp;
                    actual = actual->izq;
                    nodos++;
                }else{
                    actual->hijos = actual->hijos + 1;
                    actual = actual->izq;
                }
            } else if(actual->valor < clave) {
                if(actual->der == nullptr){
                    tmp->padre = actual;
                    actual->der = tmp;
                    actual = actual->der;
                    nodos++;
                }else{
                    actual->hijos = actual->hijos + 1;
                    actual = actual->der;
                }
            }
        }
    }
}

template <class T>
void Conjunto<T>::remover(const T& clave) {
    Nodo* actual = _raiz;
    bool encontro = false;
    while(not encontro){
        if (actual->valor < clave){
            actual->hijos = actual->hijos - 1;
            actual = actual->der;
        }else if (actual->valor > clave){
            actual->hijos = actual->hijos - 1;
            actual = actual->izq;
        }else if (actual->valor == clave){
            encontro = true;
        }else{
            break;
        }
    }
    Nodo* suplente;
    if(nodos == 1 and actual->valor == clave){
        nodos--;
        delete _raiz;
        _raiz = nullptr;
    }else if(actual->hijos == 0 && actual->valor == clave) {
        nodos--;
        delete actual;
    }else if(actual->der != nullptr){
        suplente = actual->der;
        while(suplente->izq != nullptr){
            suplente = suplente->izq;
        }
        actual->valor = suplente->valor;
        nodos--;
        delete(suplente);
    }else{
        suplente = actual->izq;
        while(suplente->der != nullptr){
            suplente = suplente->der;
        }
        actual->valor = suplente->valor;
        nodos--;
        delete(suplente);
    }
}

template <class T>
const T& Conjunto<T>::siguiente(const T& clave) {
    Nodo* actual = _raiz;
    bool encontro = false;
    while(not encontro) {
        if (actual->valor < clave) {
            actual->hijos = actual->hijos - 1;
            actual = actual->der;
        } else if (actual->valor > clave) {
            actual->hijos = actual->hijos - 1;
            actual = actual->izq;
        } else if (actual->valor == clave) {
            encontro = true;
        }
    }
    if(actual->der != nullptr){
        actual = actual->der;
        if (actual == nullptr){
            return _raiz->valor;
        }else{
            while(actual->izq != nullptr){
                actual = actual->izq;
            }
            return actual->valor;
        }
    }else{
        actual = actual->padre;
        return actual->valor;
    }
}

template <class T>
const T& Conjunto<T>::minimo() const {
    if (_raiz == nullptr){
        return _raiz->valor;
    }else{
        Nodo* actual = _raiz;
        while(actual->izq != nullptr){
            actual = actual->izq;
        }
        return actual->valor;
    }
}

template <class T>
const T& Conjunto<T>::maximo() const {
    if (_raiz == nullptr){
        return _raiz->valor;
    }else{
        Nodo* actual = _raiz;
        while(actual->der != nullptr){
            actual = actual->der;
        }
        return actual->valor;
    }
}

template <class T>
unsigned int Conjunto<T>::cardinal() const {
    return nodos;
}

template <class T>
void Conjunto<T>::mostrar(std::ostream&) const {
    assert(false);
}

template <class T>
const T& Conjunto<T>::sucesor(const T& raiz){
    Nodo* raizBuscar = raiz;
    Nodo* actual = raiz;
    if (actual != nullptr){
        return minimo(actual->der);
    }else{
        actual = actual->padre;
        while(actual != nullptr and actual->der == raizBuscar){
            raizBuscar = actual;
            actual = actual->padre;
        }
    }
    return actual;
};

template <class T>
void Conjunto<T>::destruir(struct Nodo* n) {
    if (n != nullptr) {
        destruir(n->izq);
        destruir(n->der);
        delete n;
    }
}


